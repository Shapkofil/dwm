/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const Gap default_gap        = {.isgap = 1, .realgap = 10, .gappx = 10};
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 24;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "Fira Code:style=Retina:size=8.5",
					"FiraCode Nerd Font:style=Regular:size=11",
					};
static const char dmenufont[]       = "monospace:size=10";
static const char col_text[]        = "#FFFFFF";
static const char col_glass[]       = "#000000";
static const char col_highlight[]   = "#888888";
static const char col_bordercel[]   = "#555555";
static const char col_black[]       = "#000000";
static const char col_red[]         = "#EE1A3B";
static const char col_yellow[]      = "#FFD700";
static const char col_white[]       = "#ffffff";

static const unsigned int baralpha  =   0x00;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]  = { col_text, col_glass, col_glass },
	[SchemeSel]   = { col_text, col_glass,  col_bordercel},
	[SchemeWarn]  =	{ col_yellow, col_glass, col_glass },
	[SchemeUrgent]=	{ col_red, col_glass,    col_glass  },
};
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm]  = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]   = { OPAQUE, 0x88    , borderalpha },
	[SchemeWarn]  = { OPAQUE, baralpha  , borderalpha },
	[SchemeUrgent]= { OPAQUE, baralpha  , borderalpha },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "", "", "", "" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */

	/* class                 instance  title           tags mask  isfloating monitor */
	{ "Gimp",                NULL,     NULL,             0,         1,         -1 },
	{ "Firefox",             NULL,     NULL,             1 << 8,    0,         -1 },
	{ "Spotify",             NULL,     NULL,             1 << 4,    0,          0 },
	{ NULL,                  NULL,     "Event Tester",   0,         0,         -1 }, /* xev */
	{ NULL,                  NULL,     "video0 - mpv",   0xFF,      1,         -1 }, /* mpvcam */
	{ "GLFW-Application",    NULL,     NULL,             0,         1,         -1 },

};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 0; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

#define STATUSBAR "dwmblocks"

/* commands */

static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

static const char *dmenucmd[] = { "rofi", "-dmenu", NULL};

static Key keys[] = {
	/* modifier                     key        function          argument */
	{ MODKEY|ShiftMask,             XK_b,      togglebar,        {0} },
	{ MODKEY,                       XK_j,      focusstack,       {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,       {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,       {.i = +1 } },
	{ MODKEY,                       XK_d,      incnmaster,       {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,         {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,         {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,             {0} },
	{ MODKEY,                       XK_Tab,    view,             {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,       {0} },
	{ MODKEY,                       XK_t,      setlayout,        {.v = &layouts[0]} },
	{ MODKEY,                       XK_g,      setlayout,        {.v = &layouts[1]} },
	{ MODKEY,                       XK_space,  togglefloating,   {0} },
	{ MODKEY,                       XK_f,	   togglefullscreen, {0} },
	{ MODKEY,                       XK_0,      view,             {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,              {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,         {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmonandfollow,  {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmonandfollow,  {.i = +1 } },
	{ MODKEY|ControlMask,           XK_period, tagmon,           {.i = +1 } },
	{ MODKEY|ControlMask,           XK_period, tagmon,           {.i = +1 } },
	{ MODKEY,                       XK_minus,  setgaps,          {.i = -5 } },
	{ MODKEY,                       XK_equal,  setgaps,          {.i = +5 } },
	{ MODKEY|ShiftMask,             XK_minus,  setgaps,          {.i = GAP_RESET } },
	{ MODKEY|ShiftMask,             XK_equal,  setgaps,          {.i = GAP_TOGGLE} },
	TAGKEYS(                        XK_1,                        0)
	TAGKEYS(                        XK_2,                        1)
	TAGKEYS(                        XK_3,                        2)
	TAGKEYS(                        XK_4,                        3)
	TAGKEYS(                        XK_5,                        4)
	TAGKEYS(                        XK_6,                        5)
	TAGKEYS(                        XK_7,                        6)
	TAGKEYS(                        XK_8,                        7)
	TAGKEYS(                        XK_9,                        8)
	{ MODKEY|ShiftMask,             XK_q,      quit,             {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

